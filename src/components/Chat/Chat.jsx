import React from 'react';
import s from './Chat.module.css';
import Header from '../Header/Header'
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import state from './state';

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = { users: [...state] };
  }

  sendMessage = (text) => {
    this.setState(state => {
      const users = [...state.users, {
        "id": "5",
        "text": `${text}`,
        "user": "Tanya",
        "editedAt": "",
        "avatar": "https://images.app.goo.gl/wZo65mfPQihH6W8m8",
        "userId": "5",
        "editedAt": "",
        "createdAt": `${new Date().toLocaleTimeString()}`,
        "likeCount": "0"
      }];

      return {
        users
      }
    });
  }

  toggleLike = (value) => {
    this.setState(state => {
      let users = state.users.map(user => {
        if (user['id'] === value.id) {
          if (value.isLike) {
            user['likeCount'] = (Number.parseInt(user['likeCount']) + 1).toString();
          } else {
            user['likeCount'] = Number.parseInt(user['likeCount']) - 1;
          }
        }
        return user;
      });

      return {
        users
      }
    });
  }

  render() {
    const users = this.state.users;
    const countUsers = this.state.users.map(user => user.userId);

    function uniqueVal(value, index, self) {
      return self.indexOf(value) === index;
    }

    const uniqueUsers = countUsers.filter(uniqueVal).length;
    const countMessages = this.state.users.map(user => user.text).length;
    let dates = this.state.users.map(user => user.createdAt);
    const lastDate = dates[dates.length - 1];

    return (
      <div className={s.chatWrapper}>
          <div className={s.logo}>
            <img src="http://d3ot0t2g92r1ra.cloudfront.net/img/logo@3x_optimized.svg" alt="logo" />
            Logo
          </div>
          <Header uniqueUsers={uniqueUsers} countMessages={countMessages} lastDate={lastDate} />
          <MessageList state={users} toggleLike={this.toggleLike} />
          <MessageInput sendMessage={this.sendMessage} />
          <div className={s.footer}>
            <p>Copyright</p>
          </div>
        </div>
    )
  }
}

export default Chat;