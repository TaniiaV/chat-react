import React from 'react';
import s from './Header.module.css';


class Header extends React.Component {
  render() {
    return (
      <div className={s.headerBlock}>
        <div className={s.leftBlock}>
          <div className={s.item}>
            My Chat
        </div>
          <div className={s.item}>
            {this.props.uniqueUsers} participants
        </div>
          <div className={s.item}>
            {this.props.countMessages} messages
        </div>
        </div>
        <div>
          last message at {this.props.lastDate}
        </div>
      </div>
    )
  }
}

export default Header;