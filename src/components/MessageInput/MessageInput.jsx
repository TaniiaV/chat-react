import React from 'react';
import s from './MessageInput.module.css';

class MessageInput extends React.Component {
  message;
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.message = e.target.value;
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.sendMessage(this.message);
    this.message = '';
  }

  render() {

    return (
      <div className={s.messageInputBlock}>
        <input
          onChange={this.handleChange}
          value={this.message}
          placeholder='write a message...'
          className={s.text} />
        <button
          onClick={this.handleSubmit}
          className={s.btn} >
          Send
        </button>
      </div>
    )
  }
}

export default MessageInput;