import React from 'react';
import s from './MessageItem.module.css';


class MessageItem extends React.Component {

    isLike = false;
    constructor(props) {
        super(props);
        
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
        this.isLike = !this.isLike;
        this.props.toggleLike({id: this.props.id, isLike: this.isLike});
    }

    render() {
        return (
            <div className={s.messageItem + " " + (this.props.id === "5" ? s.right : s.left)}>
                <div className={s.content}>
                    <div className={s.avatar}>
                        <img src={this.props.avatar} />
                        <div
                            className={s.name}>
                            {this.props.name}
                        </div>
                    </div>
                    <div
                        className={s.text}>
                        {this.props.message}
                    </div>
                </div>
                <div className={s.footer}>
                    <div>
                        {this.props.date}
                    </div>
                    <div 
                        className={s.likes}
                        onClick={this.handleClick} >
                        <span>likes: {this.props.like}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default MessageItem;