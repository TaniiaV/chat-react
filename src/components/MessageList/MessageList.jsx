import React from 'react';
import s from './MessageList.module.css';
import MessageItem from './MessageItem/MessageItem';

class MessageList extends React.Component {

  constructor(props) {
    super(props);
  }

  toggleLike = (value) => {
    this.props.toggleLike(value);
  }

  render() {

    let messageElements = this.props.state.map(p => 
      <MessageItem 
        id={p.id} 
        name={p.user} 
        message={p.text} 
        avatar={p.avatar} 
        date={p.createdAt} 
        like={p.likeCount} 
        toggleLike={this.toggleLike} />);

    return (
      <div className={s.messageListBlock}>
        {messageElements}
      </div>
    )
  }
}

export default MessageList;