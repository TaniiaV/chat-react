import React from 'react';
import './App.css';
import Chat from './components/Chat/Chat'

function App() {
  return (
    <div className="app-wrapper">
      <Chat />
    </div>
  );
}

export default App;
